<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use HasFactory;
    use SoftDeletes;
    
    protected $table = 'company';
    //public $incrementing = false;
	public $timestamps = false;
    protected $appends = [ 'url' ];

    //convert type data when editing and inserting
    protected $casts = [
	];

    protected $guarded = [
    ];

    public function user()
    {
        return $this->hasMany(User::class, 'id');
    }

    // ----------------------------------------------------------------------
    // Build URL based on route
    // ----------------------------------------------------------------------
    public function getUrlAttribute(){
        if( empty( $this->id )) return null;
        $url = new \stdClass;
        
        $url->create = route( 'company.create' );
        $url->edit = route( 'company.edit',  $this->id );
        $url->view = route( 'company.show', $this->id );
        $url->delete = route( 'company.destroy', $this->id );

        return $url;
    }
    // ----------------------------------------------------------------------
}
