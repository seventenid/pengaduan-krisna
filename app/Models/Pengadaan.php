<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pengadaan extends Model
{
    use HasFactory;
    use SoftDeletes;
    
    protected $table = 'pengadaan';
    //public $incrementing = false;
    protected $appends = [ 'url' ];

    //convert type data when editing and inserting
    protected $casts = [
	];

    protected $guarded = [
    ];

    // ----------------------------------------------------------------------
    // Build URL based on route
    // pengadaan dari web.php
    // ----------------------------------------------------------------------
    public function getUrlAttribute(){
        if( empty( $this->id )) return null;
        $url = new \stdClass;
        
        $url->create = route( 'pengadaan.create' );
        $url->edit = route( 'pengadaan.edit',  $this->id );
        $url->view = route( 'pengadaan.show', $this->id );
        $url->delete = route( 'pengadaan.destroy', $this->id );

        
        $url->input1 = route( 'pengadaan.input1', ['id' => $this->id] );
        $url->input2 = route( 'pengadaan.input2', ['id' => $this->id] );
        $url->input3 = route( 'pengadaan.input3', ['id' => $this->id] );
        $url->foto = route( 'pengadaan.foto', ['id' => $this->id] );

        return $url;
    }
    // ----------------------------------------------------------------------
    
    public function divisitable()
    {
        return $this->belongsTo(Divisi::class, 'dari_divisi_outlet');
    }

    public function tujuantable()
    {
        return $this->belongsTo(Divisi::class, 'ke_outlet');
    }
}
