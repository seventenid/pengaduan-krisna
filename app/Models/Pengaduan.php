<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pengaduan extends Model
{
    use HasFactory;
    use SoftDeletes;
    
    protected $table = 'pengaduan';
    //public $incrementing = false;
    protected $appends = [ 'url' ];

    //convert type data when editing and inserting
    protected $casts = [
	];

    protected $guarded = [
    ];

    // ----------------------------------------------------------------------
    // Build URL based on route
    // pengaduan dari web.php
    // ----------------------------------------------------------------------
    public function getUrlAttribute(){
        if( empty( $this->id )) return null;
        $url = new \stdClass;
        
        $url->create = route( 'pengaduan.create' );
        $url->edit = route( 'pengaduan.edit',  $this->id );
        $url->view = route( 'pengaduan.show', $this->id );
        $url->delete = route( 'pengaduan.destroy', $this->id );
        $url->estimasi = route( 'pengaduan.estimasi', $this->id );
        
        $url->input1 = route( 'pengaduan.input1', ['id' => $this->id] );

        return $url;
    }
    // ----------------------------------------------------------------------
    public function divisitable()
    {
        return $this->belongsTo(Divisi::class, 'dari_divisi_outlet');
    }

    public function tujuantable()
    {
        return $this->belongsTo(Divisi::class, 'ke_outlet');
    }
}
