<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;
use Illuminate\Database\Eloquent\SoftDeletes;

class Outlet extends Model
{
    use HasFactory;
    use SoftDeletes;
    
    protected $table = 'tujuan';
    //public $incrementing = false;
    protected $appends = [ 'url' ];

    //convert type data when editing and inserting
    protected $casts = [
	];

    protected $guarded = [
    ];

    // ----------------------------------------------------------------------
    // Build URL based on route
    // outlet dari web.php
    // ----------------------------------------------------------------------
    public function getUrlAttribute(){
        if( empty( $this->id )) return null;
        $url = new \stdClass;
        
        $url->create = route( 'outlet.create' );
        $url->edit = route( 'outlet.edit',  $this->id );
        $url->view = route( 'outlet.show', $this->id );
        $url->delete = route( 'outlet.destroy', $this->id );

        return $url;
    }
    // ----------------------------------------------------------------------

    public function user()
    {
        return $this->belongsTo(User::class, 'id');
    }

    public function pengaduan()
    {
        return $this->belongsTo(Pengaduan::class, 'id');
    }

 
}
