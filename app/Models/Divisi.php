<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;
use Illuminate\Database\Eloquent\SoftDeletes;

class Divisi extends Model
{
    use HasFactory;
    use SoftDeletes;
    
    protected $table = 'divisi';
    //public $incrementing = false;
    protected $appends = [ 'url' ];

    //convert type data when editing and inserting
    protected $casts = [
	];

    protected $guarded = [
    ];

    // ----------------------------------------------------------------------
    // Build URL based on route
    // divisi dari web.php
    // ----------------------------------------------------------------------
    public function getUrlAttribute(){
        if( empty( $this->id )) return null;
        $url = new \stdClass;
        
        $url->create = route( 'divisi.create' );
        $url->edit = route( 'divisi.edit',  $this->id );
        $url->view = route( 'divisi.show', $this->id );
        $url->delete = route( 'divisi.destroy', $this->id );

        return $url;
    }
    // ----------------------------------------------------------------------

    public function user()
    {
        return $this->belongsTo(User::class, 'id');
    }

    public function pengaduan()
    {
        return $this->belongsTo(Pengaduan::class, 'id');
    }

    public function pengaduanPending()
    {
        return $this->belongsTo(Pengaduan::class, 'id', 'ke_outlet')->where('status', 'Pending');
    }

    public function pengaduanProses()
    {
        return $this->belongsTo(Pengaduan::class, 'id', 'ke_outlet')->where('status', 'Proses');
    }

    public function pengaduanFinish()
    {
        return $this->belongsTo(Pengaduan::class, 'id', 'ke_outlet')->where('status', 'Finish');
    }

    public function pengadaanPending()
    {
        return $this->belongsTo(Pengadaan::class, 'id', 'ke_outlet')->where('status', 'Pending');
    }

    public function pengadaanFinish()
    {
        return $this->belongsTo(Pengadaan::class, 'id', 'ke_outlet')->where('status', 'Success');
    }

}
