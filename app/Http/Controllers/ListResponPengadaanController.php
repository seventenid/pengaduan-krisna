<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use DataTables;
use Auth;

use File;

use App\Models\Pengadaan;
use App\Models\Divisi;
use App\Models\Outlet;


class ListResponPengadaanController extends Controller
{
    protected function validator( array $data, $type ){
        return Validator::make($data, [
            
        ]);
    }

    public function index(Request $request)
    {
        $data = new \stdClass;
        $data->page_title       = "List Response Pengadaan";
        $data->pageConfigs      = ['pageHeader' => false];
        $data->type             = $request->type ?? "CAPEX";

        return view('content.pengadaan.listrespon', (array) $data);
    }

    public function tables(Request $request)
    {
        if ($request->ajax()) {
            $type = $request->type ?? "CAPEX";
            if(Auth::user()->roles=="Admin" || Auth::user()->roles=="Keuangan"){
                $data = Pengadaan::with('divisitable')->where('jenis', $type)->orderBy('id', 'DESC') -> get();
            }elseif(Auth::user()->roles=="Manager"){
                $dari_outlet = Auth::user()->divisi;
                $data = Pengadaan::where('dari_divisi_outlet', $dari_outlet)->where('jenis', $type)->with('divisitable')->orderBy('id', 'DESC') -> get();
            }elseif(Auth::user()->divisitable->type==0){
                $ke_outlet = Auth::user()->divisi;
                $data = Pengadaan::where('ke_outlet', $ke_outlet)->where('jenis', $type)->with('divisitable')->orderBy('id', 'DESC') -> get();
            }else{
                $data= Pengadaan::where('id', -1)->with('divisitable')->orderBy('id', 'DESC') -> get();
            }

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = "";
                    if(Auth::user()->roles=="Admin"){
                        $actionBtn = '<a href="'.$row->url->edit.'" class="edit btn btn-success btn-sm"><span class="fa fa-edit"></span>Edit</a>
                        <a data-href="'.$row->url->delete.'" class="delete delete-item btn btn-danger btn-sm" data-toggle="modal" data-target="#delete-modal"><span class="fa fa-trash"></span>Delete</a>';
                    }

                    return $actionBtn;
                })
                ->editColumn('divisi_name',function($row){
                    if($row->divisitable){
                        return $row->divisitable->divisi;
                    }

                })
                ->editColumn('outlet_name',function($row){
                    if($row->tujuantable){
                        return $row->tujuantable->divisi;
                    }

                })
                ->editColumn('note1',function($row){
                    $tombol =  '<a data-href="'.$row->url->input1.'" data-id="'.$row->id.'" class="note note-item btn btn-primary btn-sm" data-toggle="modal" data-target="#note-modal">Input</a>'; 
                    if($row->note1){
                        return $row->note1; 
                    }else{
                        if(Auth::user()->roles=="Admin" || (Auth::user()->roles=="Member" && Auth::user()->divisi==$row->ke_outlet) ){
                            return $tombol;
                        }else{
                            return "";
                        }
                    }
                })
                ->editColumn('note2',function($row){
                    $tombol =  '<a data-href="'.$row->url->input2.'" data-id="'.$row->id.'" class="note note-item btn btn-primary btn-sm" data-toggle="modal" data-target="#note-modal">Input</a>'; 
                    if($row->note2){
                        return $row->note2; 
                    }else{
                        if(Auth::user()->roles=="Admin" || (Auth::user()->roles=="Manager" && Auth::user()->divisi==$row->dari_divisi_outlet) ){
                            return $tombol;
                        }else{
                            return "";
                        }
                    }
                })
                ->editColumn('note3',function($row){
                    $tombol =  '<a data-href="'.$row->url->input3.'" data-id="'.$row->id.'" class="note note-item btn btn-primary btn-sm" data-toggle="modal" data-target="#note-modal">Input</a>'; 
                    if($row->note3){
                        return $row->note3; 
                    }else{
                        if(Auth::user()->roles=="Admin"){
                            return $tombol;
                        }elseif(Auth::user()->roles=="Keuangan"){
                            return $tombol;
                        }else{
                            return "";
                        }
                    }
                })
                ->editColumn('foto',function($row){
                    $tombol =  '<a data-href="'.$row->url->foto.'" data-id="'.$row->id.'" class="fotom fotom-item btn btn-primary btn-sm" data-toggle="modal" data-target="#fotom-modal">Input</a>'; 
                    if($row->foto){
                        return '<img src='.\URL::to($row->foto).' border="0" width="200" class="img-rounded" align="center" />'; 
                    }else{
                        if(Auth::user()->roles=="Admin" || (Auth::user()->roles=="Member" && Auth::user()->divisi==$row->ke_outlet) ){
                            return $tombol;
                        }else{
                            return "";
                        }
                    }
                })
                ->rawColumns(['action','catatan','divisi_name','outlet_name','foto', 'note1', 'note2', 'note3'])
                ->make(true);
        }
    }

}
