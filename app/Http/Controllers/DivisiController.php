<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use DataTables;

use File;

use App\Models\Divisi;

class DivisiController extends Controller
{
    protected function validator( array $data, $type ){
        return Validator::make($data, [
            
        ]);
    }

    public function index()
    {
        $data = new \stdClass;
        $data->page_title       = "Divisi List";
        $data->pageConfigs      = ['pageHeader' => false];

        return view('content.divisi.list', (array) $data);
    }

    public function show($id){
        $data = new \stdClass;
        $data->item             = Divisi::findOrFail($id);
        $data->page_title       = "Divisi View";
        $data->page_type        = "view";
        $data->form_action      = route('divisi.update', $id);
        $data->pageConfigs      = ['pageHeader' => false];

        return view('content.divisi.form', (array) $data);
    }

    public function tables(Request $request)
    {
        if ($request->ajax()) {
            $data = Divisi::get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="'.$row->url->view.'" class="edit btn btn-primary btn-sm"><span class="flaticon2-graph-1"></span>Detail</a> 
                                  <a href="'.$row->url->edit.'" class="edit btn btn-success btn-sm"><span class="fa fa-edit"></span>Edit</a>
                                  <a data-href="'.$row->url->delete.'" class="delete delete-item btn btn-danger btn-sm" data-toggle="modal" data-target="#delete-modal"><span class="fa fa-trash"></span>Delete</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function create(){
        $data = new \stdClass;
        $data->item             = new Divisi();
        $data->page_title       = "Divisi Create";
        $data->page_type        = "create";
        $data->form_action      = route('divisi.store');
        $data->pageConfigs      = ['pageHeader' => false];

        //options
      

        return view('content.divisi.form', (array) $data);
    }

    public function store(Request $request){
        $data = $request->all();
        $this->validator($data, 'create')->validate();

        $insert = new Divisi();
        $insert->fill($data)->save();

        return redirect()->route('divisi.index')->with('success', "Success Creating Data");
    }

    public function edit($id){
        $data = new \stdClass;
        $data->item             = Divisi::findOrFail($id);
        $data->page_title       = "Divisi Edit";
        $data->page_type        = "edit";
        $data->form_action      = route('divisi.update', $id);
        $data->pageConfigs      = ['pageHeader' => false];

        //For Image Display
        return view('content.divisi.form', (array) $data);
    }

    public function update(Request $request, $id){
        $data = $request->all();
        $currentData        = Divisi::findOrFail($id);
        $data['id']         = $id;
        $this->validator($data, 'update')->validate();

        $currentData->update($data);

        return redirect()->route('divisi.index')->with('success', "Success Update Data");
    }

    public function destroy($id){
        $data = new \stdClass;
        try {
            $item = Divisi::findOrFail($id);

            $item->delete();
            
            $data->type = "Success";
            $data->msg = "Success Delete Data";
            return response()->json(($data), 200);
        } catch(\Exception $exception){
            $data->type = "Error";
            $data->msg = "Something Went Wrong Please Try Again !";
            return response()->json(($data), 200);
        }
    }
}
