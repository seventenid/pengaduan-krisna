<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use DataTables;
use Auth;

use File;

use App\Models\Pengaduan;
use App\Models\Divisi;

class PengaduanController extends Controller
{
    protected function validator( array $data, $type ){
        return Validator::make($data, [
            
        ]);
    }

    public function index()
    {
        $data = new \stdClass;
        $data->page_title       = "Pengaduan List";
        $data->pageConfigs      = ['pageHeader' => false];

        return view('content.pengaduan.list', (array) $data);
    }

    public function show($id){
        $data = new \stdClass;
        $data->item             = Pengaduan::findOrFail($id);
        $data->page_title       = "Pengaduan View";
        $data->page_type        = "view";
        $data->form_action      = route('pengaduan.update', $id);
        $data->pageConfigs      = ['pageHeader' => false];

        $data->divisi_options  = Divisi::pluck('divisi', 'id');
        $data->tujuan_options  = Divisi::where('type', 0)->pluck('divisi', 'id');

        return view('content.pengaduan.form', (array) $data);
    }

    public function tables(Request $request)
    {
        if ($request->ajax()) {

            if(Auth::user()->roles=="Admin"){
                $data = Pengaduan::with('divisitable')->orderBy('id', 'DESC') -> get();
            }else{
                $dariDivisi = Auth::user()->divisi;
                $data = Pengaduan::where('dari_divisi_outlet', $dariDivisi)->with('divisitable')->orderBy('id', 'DESC') -> get();
            }

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = "";
                    if(Auth::user()->roles=="Admin"){
                        $actionBtn = '<a href="'.$row->url->edit.'" class="edit btn btn-success btn-sm"><span class="fa fa-edit"></span>Edit</a>
                        <a data-href="'.$row->url->delete.'" class="delete delete-item btn btn-danger btn-sm" data-toggle="modal" data-target="#delete-modal"><span class="fa fa-trash"></span>Delete</a>
                        <a data-href="'.$row->id.'" class="delete estimasi-item btn btn-danger btn-sm" data-toggle="modal" data-target="#estimasi-modal"><span class="fa fa-trash"></span>Estimasi</a>
                        <a href="'. url('pengaduan/done/'.$row->id) .'" class="edit btn btn-success btn-sm"><span class="fa fa-edit"></span>Done</a>';
                    }
                    return $actionBtn;
                })
                ->editColumn('divisi_name',function($row){
                    if($row->divisitable){
                        return $row->divisitable->divisi;
                    }

                })
                ->editColumn('outlet_name',function($row){
                    if($row->tujuantable){
                        return $row->tujuantable->divisi;
                    }

                })
                ->editColumn('foto',function($row){
                    if($row->foto){
                        
                        return '<img src='.$row->foto.' border="0" width="200" class="img-rounded" align="center" />'; 
                    }

                })
                ->rawColumns(['action','keterangan','divisi_name','outlet_name','foto'])
                ->make(true);
        }
    }

    public function create(){
        $data = new \stdClass;
        $data->item             = new Pengaduan();
        $data->page_title       = "Pengaduan Create";
        $data->page_type        = "create";
        $data->form_action      = route('pengaduan.store');
        $data->pageConfigs      = ['pageHeader' => false];

        //options
        $data->divisi_options  = Divisi::pluck('divisi', 'id');
        $data->tujuan_options  = Divisi::where('type', 0)->pluck('divisi', 'id');

        return view('content.pengaduan.form', (array) $data);
    }

    public function store(Request $request){
        $data = $request->except('divisi');
        $this->validator($data, 'create')->validate();

        $data['status'] = "Pending";

        //Upload Image Progress
        if(isset($data['foto'])){
            $file = $request->file('foto');
            $file_name = $file->getClientOriginalName();
            $filename = date('YmdHis') .'_'. str_replace(' ', '', $data['nama'])  .'_'. $file_name;

            Storage::disk('public_folder')->putFileAs('images/pengaduan', $file, $filename);

            $data['foto'] = "images/pengaduan/".$filename;
        }
        //-----------------------------

        $insert = new Pengaduan();
        $insert->fill($data)->save();

        /*$to_name = $data['nama'];
        $to_email = "akun110195@gmail.com";
        $data = array("name"=>"", "body" => "");
        $data['test'] = User::where()->get();
        Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email) {
        $message->to($to_email, $to_name)
        ->subject("Pengaduan");
        $message->from("rullyrizal@gmail.com");
        });*/

        
        $email = Divisi::where('id', $insert->ke_outlet)->first()->email;
        \Mail::to($email)->send(new \App\Mail\pengaduanEmail($data));

        return redirect()->route('pengaduan.index')->with('success', "Success Creating Data");
    }

    public function edit($id){
        $data = new \stdClass;
        $data->item             = Pengaduan::findOrFail($id);
        $data->page_title       = "Pengaduan Edit";
        $data->page_type        = "edit";
        $data->form_action      = route('pengaduan.update', $id);
        $data->pageConfigs      = ['pageHeader' => false];

       //options
        $data->divisi_options  = Divisi::pluck('divisi', 'id');
        $data->tujuan_options  = Divisi::where('type', 0)->pluck('divisi', 'id');

        return view('content.pengaduan.form', (array) $data);
    }

    public function update(Request $request, $id){
        $data = $request->all();
        $currentData        = Pengaduan::findOrFail($id);
        $data['id']         = $id;
        $this->validator($data, 'update')->validate();

        //Upload Image Progress
        if(isset($data['foto'])){
            $file = $request->file('foto');
            $file_name = $file->getClientOriginalName();
            $filename = date('YmdHis') .'_'. str_replace(' ', '', $data['id'])  .'_'. $file_name;

            Storage::disk('custom_folder_1')->putFileAs('assets/images', $file, $filename);



            $data['foto'] = "assets/images/".$filename;
        }
        //-----------------------------

        $currentData->update($data);

        return redirect()->route('pengaduan.index')->with('success', "Success Update Data");
    }

    public function destroy($id){
        $data = new \stdClass;
        try {
            $item = Pengaduan::findOrFail($id);

            //delete file first before updating
            $filename = $item->foto;
            if(Storage::disk('public')->exists($filename)) {
                Storage::disk('public')->delete($filename);
            }

            $item->delete();
            
            $data->type = "Success";
            $data->msg = "Success Delete Data";
            return response()->json(($data), 200);
        } catch(\Exception $exception){
            $data->type = "Error";
            $data->msg = "Something Went Wrong Please Try Again !";
            return response()->json(($data), 200);
        }
    }

    public function done($id){
        $data = [];
        $response = new \stdClass;
        try {
            $item = Pengaduan::findOrFail($id);
            $data['status'] = "Finish";
            $data['donedate']=Carbon::now();
            $item->update($data);
            
            return redirect()->back()->with('success', "Success Update Data");
            
        } catch(\Exception $exception){
            $response->type = "Error";
            $response->msg = "Something Went Wrong Please Try Again !";
            return response()->json(($response), 200);
           
        }
    }

    public function estimasi(Request $request){
        $response = new \stdClass;
        try {
            $data = [];
            $request = $request->all();
            $item = Pengaduan::findOrFail($request['id']);
            $data['estimasi'] = $request['estimasi'];
            $data['status'] = "Proses";
            $item->update($data);
                
            $response->type = "Success";
            $response->msg = "Success Delete Data";
            return response()->json(($response), 200);
        } catch(\Exception $exception){
            $response->type = "Error";
            $response->msg = "Something Went Wrong Please Try Again !";
            return response()->json(($response), 200);
        }
    }
}
