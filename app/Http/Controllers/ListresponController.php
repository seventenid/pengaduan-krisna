<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use DataTables;
use Auth;

use File;

use App\Models\Pengaduan;
use App\Models\Divisi;
use App\Models\Outlet;


class ListresponController extends Controller
{
    protected function validator( array $data, $type ){
        return Validator::make($data, [
            
        ]);
    }

    public function index()
    {
        $data = new \stdClass;
        $data->page_title       = "List Response";
        $data->pageConfigs      = ['pageHeader' => false];

        return view('content.pengaduan.listrespon', (array) $data);
    }

    public function tables(Request $request)
    {
        if ($request->ajax()) {

            if(Auth::user()->roles=="Admin"){
                $data = Pengaduan::with('divisitable')->orderBy('id', 'DESC') -> get();
            }elseif(Auth::user()->divisitable->type==0){
                $ke_outlet = Auth::user()->divisi;
                $data = Pengaduan::where('ke_outlet', $ke_outlet)->with('divisitable')->orderBy('id', 'DESC') -> get();
            }else{
                $data= Pengaduan::where('id', -1)->with('divisitable')->orderBy('id', 'DESC') -> get();
            }

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = "";
                    if(Auth::user()->roles=="Admin"){
                        $actionBtn = '<a href="'.$row->url->edit.'" class="edit btn btn-success btn-sm"><span class="fa fa-edit"></span>Edit</a>
                        <a data-href="'.$row->url->delete.'" class="delete delete-item btn btn-danger btn-sm" data-toggle="modal" data-target="#delete-modal"><span class="fa fa-trash"></span>Delete</a>
                        <a data-href="'.$row->id.'" class="delete estimasi-item btn btn-danger btn-sm" data-toggle="modal" data-target="#estimasi-modal"><span class="fa fa-trash"></span>Estimasi</a>
                        <a href="'. url('pengaduan/done/'.$row->id) .'" class="edit btn btn-success btn-sm"><span class="fa fa-edit"></span>Done</a>';
                    }elseif(Auth::user()->roles=="Member"){
                        $actionBtn = '<a data-href="'.$row->id.'" class="delete estimasi-item btn btn-danger btn-sm" data-toggle="modal" data-target="#estimasi-modal"><span class="fa fa-trash"></span>Estimasi</a>
                        <a href="'. url('pengaduan/done/'.$row->id) .'" class="edit btn btn-success btn-sm"><span class="fa fa-edit"></span>Done</a>
                        <a data-href="'.$row->url->input1.'" data-id="'.$row->id.'" class="note note-item btn btn-primary btn-sm" data-toggle="modal" data-target="#note-modal">Note</a>';
                    }

                    return $actionBtn;
                })
                ->editColumn('divisi_name',function($row){
                    if($row->divisitable){
                        return $row->divisitable->divisi;
                    }

                })
                ->editColumn('outlet_name',function($row){
                    if($row->tujuantable){
                        return $row->tujuantable->divisi;
                    }

                })
                ->editColumn('foto',function($row){
                    if($row->foto){
                        
                        return '<img src='.\URL::to($row->foto).' border="0" width="200" class="img-rounded" align="center" />'; 
                    }

                })
                ->rawColumns(['action','keterangan','divisi_name','outlet_name','foto'])
                ->make(true);
        }
    }


    public function input1(Request $request){
        $data = [];
        $response = new \stdClass;
        try {
            $item = Pengaduan::findOrFail($request->id);
            $data['note1']=$request->note;

            $item->update($data);
            
            $response->type = "Success";
            $response->msg = "Success Update Data";
            
        } catch(\Exception $exception){
            $response->type = "Error";
            $response->msg = $exception->getMessage();
        }
        return response()->json(($response), 200);
    }

}
