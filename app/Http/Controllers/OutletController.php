<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use DataTables;

use File;

use App\Models\Outlet;

class OutletController extends Controller
{
    protected function validator( array $data, $type ){
        return Validator::make($data, [
            
        ]);
    }

    public function index()
    {
        $data = new \stdClass;
        $data->page_title       = "Outlet List";
        $data->pageConfigs      = ['pageHeader' => false];

        return view('content.outlet.list', (array) $data);
    }

    public function show($id){
        $data = new \stdClass;
        $data->item             = Outlet::findOrFail($id);
        $data->page_title       = "Outlet View";
        $data->page_type        = "view";
        $data->form_action      = route('outlet.update', $id);
        $data->pageConfigs      = ['pageHeader' => false];

        return view('content.outlet.form', (array) $data);
    }

    public function tables(Request $request)
    {
        if ($request->ajax()) {
            $data = Outlet::get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="'.$row->url->view.'" class="edit btn btn-primary btn-sm"><span class="flaticon2-graph-1"></span>Detail</a> 
                                  <a href="'.$row->url->edit.'" class="edit btn btn-success btn-sm"><span class="fa fa-edit"></span>Edit</a>
                                  <a data-href="'.$row->url->delete.'" class="delete delete-item btn btn-danger btn-sm" data-toggle="modal" data-target="#delete-modal"><span class="fa fa-trash"></span>Delete</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function create(){
        $data = new \stdClass;
        $data->item             = new Outlet();
        $data->page_title       = "Outlet Create";
        $data->page_type        = "create";
        $data->form_action      = route('outlet.store');
        $data->pageConfigs      = ['pageHeader' => false];

        //options
        $data->company_options  = Outlet::pluck('id', 'id');

        return view('content.outlet.form', (array) $data);
    }

    public function store(Request $request){
        $data = $request->all();
        $this->validator($data, 'create')->validate();

        $insert = new Outlet();
        $insert->fill($data)->save();

        return redirect()->route('outlet.index')->with('success', "Success Creating Data");
    }

    public function edit($id){
        $data = new \stdClass;
        $data->item             = Outlet::findOrFail($id);
        $data->page_title       = "Outlet Edit";
        $data->page_type        = "edit";
        $data->form_action      = route('outlet.update', $id);
        $data->pageConfigs      = ['pageHeader' => false];

        //For Image Display
        return view('content.outlet.form', (array) $data);
    }

    public function update(Request $request, $id){
        $data = $request->all();
        $currentData        = Outlet::findOrFail($id);
        $data['id']         = $id;
        $this->validator($data, 'update')->validate();

        $currentData->update($data);

        return redirect()->route('outlet.index')->with('success', "Success Update Data");
    }

    public function destroy($id){
        $data = new \stdClass;
        try {
            $item = Outlet::findOrFail($id);

            //delete file first before updating
            $filename = $item->foto;
            if(Storage::disk('public')->exists($filename)) {
                Storage::disk('public')->delete($filename);
            }

            $item->delete();
            
            $data->type = "Success";
            $data->msg = "Success Delete Data";
            return response()->json(($data), 200);
        } catch(\Exception $exception){
            $data->type = "Error";
            $data->msg = "Something Went Wrong Please Try Again !";
            return response()->json(($data), 200);
        }
    }
}
