<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use DataTables;
use Auth;

use File;

use App\Models\Pengadaan;
use App\Models\Divisi;

class PengadaanController extends Controller
{
    protected function validator( array $data, $type ){
        return Validator::make($data, [
            
        ]);
    }

    public function index(Request $request)
    {
        $data = new \stdClass;
        $data->page_title       = "Pengadaan List";
        $data->pageConfigs      = ['pageHeader' => false];
        $data->type             = $request->type ?? "CAPEX";

        return view('content.pengadaan.list', (array) $data);
    }

    public function show($id){
        $data = new \stdClass;
        $data->item             = Pengadaan::findOrFail($id);
        $data->page_title       = "Pengadaan View";
        $data->page_type        = "view";
        $data->form_action      = route('pengadaan.update', $id);
        $data->pageConfigs      = ['pageHeader' => false];

        return view('content.pengadaan.form', (array) $data);
    }

    public function tables(Request $request)
    {
        $type = $request->type ?? "CAPEX";
        if ($request->ajax()) {

            if(Auth::user()->roles=="Admin"){
                $data = Pengadaan::where('jenis', $type)->with('divisitable')->orderBy('id', 'DESC') -> get();
            }else{
                $dariDivisi = Auth::user()->divisi;
                $data = Pengadaan::where('jenis', $type)->where('dari_divisi_outlet', $dariDivisi)->with('divisitable')->orderBy('id', 'DESC') -> get();
            }

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                    if(Auth::user()->roles=="Admin"){
                        $actionBtn = '<a href="'.$row->url->view.'" class="edit btn btn-primary btn-sm"><span class="flaticon2-graph-1"></span>Detail</a> 
                                    <a href="'.$row->url->edit.'" class="edit btn btn-success btn-sm"><span class="fa fa-edit"></span>Edit</a>
                                    <a data-href="'.$row->url->delete.'" class="delete delete-item btn btn-danger btn-sm" data-toggle="modal" data-target="#delete-modal"><span class="fa fa-trash"></span>Delete</a>';
                        return $actionBtn;
                    }

                })
                ->editColumn('divisi_name',function($row){
                    if($row->divisitable){
                        return $row->divisitable->divisi;
                    }

                })
                ->editColumn('outlet_name',function($row){
                    if($row->tujuantable){
                        return $row->tujuantable->divisi;
                    }

                })
                ->editColumn('foto',function($row){
                    if($row->foto){
                        return '<img src='.$row->foto.' border="0" width="200" class="img-rounded" align="center" />'; 
                    }
                })
                ->rawColumns(['action', 'catatan','divisi_name','outlet_name','foto', 'note1', 'note2', 'note3'])
                ->make(true);
        }
    }

    public function create(){
        $data = new \stdClass;
        $data->item             = new Pengadaan();
        $data->page_title       = "Pengadaan Create";
        $data->page_type        = "create";
        $data->form_action      = route('pengadaan.store');
        $data->pageConfigs      = ['pageHeader' => false];

        //options
        $data->divisi_options  = Divisi::pluck('divisi', 'id');
        $data->tujuan_options  = Divisi::where('type', 0)->pluck('divisi', 'id');

        return view('content.pengadaan.form', (array) $data);
    }

    public function store(Request $request){
        $data = $request->except('divisi');
        $this->validator($data, 'create')->validate();

        $data['status'] = "Pending";
        $data['bydivisi'] = "Pending";
        $data['bymanager'] = "Pending";
        $data['bykeuangan'] = "Pending";

        $insert = new Pengadaan();
        $insert->fill($data)->save();

        $email = Divisi::where('id', $insert->ke_outlet)->first()->email;

        \Mail::to($email)->send(new \App\Mail\pengadaanEmail($data));

        return redirect()->route('pengadaan.index')->with('success', "Success Creating Data");
    }

    public function edit($id){
        $data = new \stdClass;
        $data->item             = Pengadaan::findOrFail($id);
        $data->page_title       = "Pengadaan Edit";
        $data->page_type        = "edit";
        $data->form_action      = route('pengadaan.update', $id);
        $data->pageConfigs      = ['pageHeader' => false];

        $data->divisi_options  = Divisi::pluck('divisi', 'id');
        $data->tujuan_options  = Divisi::where('type', 0)->pluck('divisi', 'id');
        //For Image Display
        return view('content.pengadaan.form', (array) $data);
    }

    public function update(Request $request, $id){
        $data = $request->except('divisi');
        $currentData        = Pengadaan::findOrFail($id);
        $data['id']         = $id;
        $this->validator($data, 'update')->validate();

        $currentData->update($data);

        return redirect()->route('pengadaan.index')->with('success', "Success Update Data");
    }

    public function destroy($id){
        $data = new \stdClass;
        try {
            $item = Pengadaan::findOrFail($id);

            //delete file first before updating
            $filename = $item->logo;
            if(Storage::disk('custom_folder_1')->exists($filename)) {
                Storage::disk('custom_folder_1')->delete($filename);
            }

            $item->delete();
            
            $data->type = "Success";
            $data->msg = "Success Delete Data";
            return response()->json(($data), 200);
        } catch(\Exception $exception){
            $data->type = "Error";
            $data->msg = "Something Went Wrong Please Try Again !";
            return response()->json(($data), 200);
        }
    }

    public function input1(Request $request){
        $data = [];
        $response = new \stdClass;
        try {
            $item = Pengadaan::findOrFail($request->id);
            $data['bydivisi'] = "Success";
            $data['note1']=$request->note;

            if($item->bymanager=="Success" && $item->bykeuangan=="Success"){
                $data['status']="Success";
            }

            $item->update($data);
            
            $response->type = "Success";
            $response->msg = "Success Update Data";
            
        } catch(\Exception $exception){
            $response->type = "Error";
            $response->msg = $exception->getMessage();
        }
        return response()->json(($response), 200);
    }

    public function input2(Request $request){
        $data = [];
        $response = new \stdClass;
        try {
            $item = Pengadaan::findOrFail($request->id);
            $data['bymanager'] = "Success";
            $data['note2']=$request->note;

            if($item->bydivisi=="Success" && $item->bykeuangan=="Success"){
                $data['status']="Success";
            }

            $item->update($data);
            
            $response->type = "Success";
            $response->msg = "Success Update Data";
            
        } catch(\Exception $exception){
            $response->type = "Error";
            $response->msg = "Something Went Wrong Please Try Again !";
        }
        return response()->json(($response), 200);
    }

    public function input3(Request $request){
        $data = [];
        $response = new \stdClass;
        try {
            $item = Pengadaan::findOrFail($request->id);
            $data['bykeuangan'] = "Success";
            $data['note3']=$request->note;

            if($item->bydivisi=="Success" && $item->bymanager=="Success"){
                $data['status']="Success";
            }

            $item->update($data);
            
            $response->type = "Success";
            $response->msg = "Success Update Data";
            
        } catch(\Exception $exception){
            $response->type = "Error";
            $response->msg = "Something Went Wrong Please Try Again !";
        }
        return response()->json(($response), 200);
    }

    public function foto(Request $request){
        $data = [];
        $response = new \stdClass;
        try {
            $item = Pengadaan::findOrFail($request->id);

            //Upload Image Progress
            if ($request->file('foto')) {
                $file = $request->file('foto');
                $file_name = $file->getClientOriginalName();
                $filename = date('YmdHis') .'_'. str_replace(' ', '', $item->nama)  .'_'. $file_name;

                Storage::disk('public_folder')->putFileAs('images/pengadaan', $file, $filename);

                $data['foto'] = "images/pengadaan/".$filename;
            }
            //-----------------------------

            $item->update($data);
            
            $response->type = "Success";
            $response->msg = "Success Update Data";
            
        } catch(\Exception $exception){
            $response->type = "Error";
            $response->msg = $exception->getMessage();
        }
        return response()->json(($response), 200);
    }
}
