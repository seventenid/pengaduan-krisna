<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use DataTables;

use File;

use App\Models\Company;

class CompanyController extends Controller
{
    protected function validator( array $data, $type ){
        return Validator::make($data, [
            'name'          => 'required|max:255',
        ]);
    }

    public function index()
    {
        $data = new \stdClass;
        $data->page_title       = "Company List";
        $data->pageConfigs      = ['pageHeader' => false];

        return view('content.company.list', (array) $data);
    }

    public function show($id){
        $data = new \stdClass;
        $data->item             = Company::findOrFail($id);
        $data->page_title       = "Company View";
        $data->page_type        = "view";
        $data->form_action      = route('company.update', $id);
        $data->pageConfigs      = ['pageHeader' => false];

        return view('content.company.form', (array) $data);
    }

    public function tables(Request $request)
    {
        if ($request->ajax()) {
            $data = Company::get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="'.$row->url->view.'" class="edit btn btn-primary btn-sm"><span class="flaticon2-graph-1"></span>Detail</a> 
                                  <a href="'.$row->url->edit.'" class="edit btn btn-success btn-sm"><span class="fa fa-edit"></span>Edit</a>';
                    return $actionBtn;
                })
                ->editColumn('status', function ($row) {
                    switch ($row->status) {
                        case 1:
                            return "Active";
                            break;
                        case 0:
                            return "Deactive";
                            break;
                    }
                })
                ->rawColumns(['action','description'])
                ->make(true);
        }
    }

    public function create(){
    }

    public function store(Request $request){
    }

    public function edit($id){
        $data = new \stdClass;
        $data->item             = Company::findOrFail($id);
        $data->page_title       = "Company Edit";
        $data->page_type        = "edit";
        $data->form_action      = route('company.update', $id);
        $data->pageConfigs      = ['pageHeader' => false];

        //For Image Display
        return view('content.company.form', (array) $data);
    }

    public function update(Request $request, $id){
        $data = $request->all();
        $currentData        = Company::findOrFail($id);
        $data['id']         = $id;
        $this->validator($data, 'update')->validate();

        //Upload Image Progress
        if(isset($data['logo'])){
            $file = $request->file('logo');
            $file_name = $file->getClientOriginalName();
            $filename = date('YmdHis') .'_'. str_replace(' ', '', $data['name'])  .'_'. $file_name;

            Storage::disk('custom_folder_1')->putFileAs('assets/images', $file, $filename);

            if(Storage::disk('custom_folder_1')->exists($currentData->logo)) {
                Storage::disk('custom_folder_1')->delete($currentData->logo);
            }

            $data['logo'] = "assets/images/".$filename;
        }
        //-----------------------------

        $currentData->update($data);

        return redirect()->route('company.index')->with('success', "Success Update Data");
    }

    public function destroy($id){
        $data = new \stdClass;
        try {
            $item = Company::findOrFail($id);

            //delete file first before updating
            $filename = $item->logo;
            if(Storage::disk('custom_folder_1')->exists($filename)) {
                Storage::disk('custom_folder_1')->delete($filename);
            }

            $item->delete();
            
            $data->type = "Success";
            $data->msg = "Success Delete Data";
            return response()->json(($data), 200);
        } catch(\Exception $exception){
            $data->type = "Error";
            $data->msg = "Something Went Wrong Please Try Again !";
            return response()->json(($data), 200);
        }
    }
}
