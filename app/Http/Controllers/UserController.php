<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use DataTables;

 
use File;
use Auth;


use App\Models\User;
use App\Models\Divisi;
use App\Models\Outlet;

class UserController extends Controller
{
    protected function validator( array $data, $type ){
        return Validator::make($data, [
            'email'         => 'required|max:255|unique:users,email' . ($type == 'update' ? ','.$data['id'] : ''),
            'name'          => 'required|max:255',
            'password'      => 'required|min:8',
        ]);
    }

    public function index()
    {
        $data = new \stdClass;
        $data->page_title       = "User List";
        $data->pageConfigs      = ['pageHeader' => false];

        return view('content.user.list', (array) $data);
    }

    public function show($id){
        $data = new \stdClass;
        $data->item             = User::findOrFail($id);
        $data->page_title       = "User View";
        $data->page_type        = "view";
        $data->form_action      = route('user.update', $id);
        $data->pageConfigs      = ['pageHeader' => false];

        
        //options
        $data->divisi_options  = Divisi::pluck('divisi', 'id');

        return view('content.user.form', (array) $data);
    }

    public function tables(Request $request)
    {
        if ($request->ajax()) {
            $data = User::with('divisitable') -> get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="'.$row->url->view.'" class="edit btn btn-primary btn-sm"><span class="flaticon2-graph-1"></span>Detail</a> 
                                  <a href="'.$row->url->edit.'" class="edit btn btn-success btn-sm"><span class="fa fa-edit"></span>Edit</a> 
                                  <a data-href="'.$row->url->delete.'" class="delete delete-item btn btn-danger btn-sm" data-toggle="modal" data-target="#delete-modal"><span class="fa fa-trash"></span>Delete</a>';
                    return $actionBtn;
              
                })
                ->editColumn('divisi_name',function($row){
                    if($row->divisitable){
                        return $row->divisitable->divisi;
                    }

                })
                ->rawColumns(['action','divisi_name'])
                ->make(true);
                
        }
    }

    public function create(){
        $data = new \stdClass;
        $data->item             = new User();
        $data->page_title       = "User Create";
        $data->page_type        = "create";
        $data->form_action      = route('user.store');
        $data->pageConfigs      = ['pageHeader' => false];

        //options
        $data->divisi_options  = Divisi::pluck('divisi', 'id')->prepend('--Select Data--','');

        return view('content.user.form', (array) $data);
    }

    public function store(Request $request){
        $data = $request->all();
        $data['password']   = $data['password'];
        $this->validator($data, 'create')->validate();

        if(Hash::needsRehash($data['password']) && !empty($data['password'])){
            $data['password'] = bcrypt($data['password']);
        }

        $insert = new User();
        $insert->fill($data)->save();

        return redirect()->route('user.index')->with('success', "Success Creating Data");
    }

    public function edit($id){
        $data = new \stdClass;
        $data->item             = User::findOrFail($id);
        $data->page_title       = "User Edit";
        $data->page_type        = "edit";
        $data->form_action      = route('user.update', $id);
        $data->pageConfigs      = ['pageHeader' => false];

        //options
        $data->divisi_options  = Divisi::pluck('divisi', 'id')->prepend('--Select Data--','');

        return view('content.user.form', (array) $data);
    }

    public function update(Request $request, $id){
        $data = $request->all();
        $currentData        = User::findOrFail($id);
        $data['id']         = $id;
        $data['password']   = !empty($data['password']) ? $data['password'] : $currentData['password'];
        $this->validator($data, 'update')->validate();

        if(Hash::needsRehash($data['password']) && !empty($data['password'])){
            $data['password'] = bcrypt($data['password']);
        }

        $currentData->update($data);

        return redirect()->route('user.index')->with('success', "Success Update Data");
    }

    public function destroy($id){
        $data = new \stdClass;
        try {
            $userId = Auth::id();

            if($id == $userId){
                $data->type = "Error";
                $data->msg = "Cannot Delete Your Own Data";
                return response()->json(($data), 200);
            }

            $item = User::findOrFail($id);
            $item->delete();
            
            $data->type = "Success";
            $data->msg = "Success Delete Data";
            return response()->json(($data), 200);
        } catch(\Exception $exception){
            $data->type = "Error";
            $data->msg = "Something Went Wrong Please Try Again !";
            return response()->json(($data), 200);
        }
    }

    
}
