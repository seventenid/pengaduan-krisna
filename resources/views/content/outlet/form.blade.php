@extends('layouts/contentLayoutMaster')

@section('title')
  {{$page_title}}
@endsection

@section('vendor-style')
  {{-- Vendor Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-user.css')) }}">

  <link rel="stylesheet" href="{{ asset('css/summernote/summernote.min.css') }}">
@endsection

@section('content')
<!-- users edit start -->
<section class="app-user-edit">
  <div class="card">
    <div class="card-body">

      <h1>Form Outlet</h1>
      </br></br>

        <!-- Form1 Tab starts -->
        <div class="tab-pane active" id="form1" aria-labelledby="form1-tab" role="tabpanel">

          @include("_includes.alert")

          @php $disabled = $page_type == 'view' ? 'disabled' : '' @endphp

          <!-- users edit account form start -->
          <form class="form-validate" id="Form_Input" method="{{ !empty($method) ? $method : 'POST' }}" action="{{ $form_action }}" enctype="multipart/form-data">
            @csrf
            @if(empty($method))
              {{ $page_type == 'create' ? '' : method_field('PUT') }}
            @endif

           
            <div class="row">
            
              <div class="col-md-4">
                <div class="form-group">
                  <label for="outlet">Outlet</label>
                  <input
                    type="text"
                    class="form-control @error('outlet') is-invalid @enderror"
                    value="{{ !empty($item->outlet) ? $item->outlet : old('outlet') }}"
                    name="outlet"
                    id="outlet"
                    {{ $disabled }}
                  />
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label for="manager">Manager</label>
                  <input
                    type="text"
                    class="form-control @error('manager') is-invalid @enderror"
                    value="{{ !empty($item->manager) ? $item->manager : old('manager') }}"
                    name="manager"
                    id="manager"
                    {{ $disabled }}
                  />
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label for="email">Email</label>
                  <input
                    type="text"
                    class="form-control @error('email') is-invalid @enderror"
                    value="{{ !empty($item->email) ? $item->outemaillet : old('email') }}"
                    name="email"
                    id="email"
                    {{ $disabled }}
                  />
                </div>
              </div>
              
              <div class="col-12 d-flex flex-sm-row flex-column mt-2">
                @if ($page_type == "edit" || $page_type == "create")
                <button type="submit" class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1">{{ $page_type == 'create' ? 'Create Outlet' : 'Save Changes' }}</button>
                <button type="reset" class="btn btn-outline-secondary mb-1 mb-sm-0 mr-0 mr-sm-1">Reset</button>
                @endif
                
              </div>
            </div>
          </form>
          <!-- users edit account form ends -->
        </div>
        <!-- Form1 Tab ends -->

      </div>

    </div>
  </div>
</section>
<!-- users edit ends -->
@endsection

@section('vendor-script')
  {{-- Vendor js files --}}
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/components/components-navs.js')) }}"></script>
  <script src="{{ asset('css/summernote/summernote.min.js') }}"></script>

  <script>
    $(document).ready(function() {
      $('.summernote').summernote();
    });
  </script>

  <script type="text/javascript">
    $(function () {
      'use strict';

      //Select2-------------------------
      $(".basic-select2").select2();
      //Select2-------------------------

      // Change user profile picture-------------------------
      var changePicture = $('#change-picture');
      var userAvatar = $('.user-avatar');

      if (changePicture.length) {
        $(changePicture).on('change', function (e) {
          var reader = new FileReader(),
            files = e.target.files;
          reader.onload = function () {
            if (userAvatar.length) {
              userAvatar.attr('src', reader.result);
            }
          };
          reader.readAsDataURL(files[0]);
        });
      }
      // Change user profile picture-------------------------

      // Validation-------------------------
      var form = $('.form-validate');
      if (form.length) {
        $(form).each(function () {
          var $this = $(this);
          $this.validate({
            submitHandler: function (form, event) {
              form.submit();
            },
            rules: {
              name: {
                required: true
              },
            }
          });
        });
      }
      // Validation-------------------------
      
    });
  </script>
@endsection
