@extends('layouts/contentLayoutMaster')

@section('title')
  {{$page_title}}
@endsection

@section('vendor-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap4.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
@endsection

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-user.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
@endsection

@section('content')
<!-- users list start -->
<section class="app-user-list">
  <!-- list section start -->
  <div class="card">

    @include("_includes.alert")

    <!--begin::Header-->
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
      <div class="card-title">
        <h3 class="card-label">{{$page_title}}</h3>
      </div>
      <div class="card-toolbar">
        <!--begin::Button-->
        <!--<a href="{{route('company.create')}}" class="btn btn-primary font-weight-bolder">
        <span class="fa fa-plus-circle"></span> Add Data</a>-->
        <!--end::Button-->
      </div>
    </div>
    <!--end::Header-->


    <!--begin::Body-->
    <div class="card-body">
      <div class="card-datatable table-responsive">
        <table class="table table-bordered yajra-datatable">
          <thead class="thead-light">
            <tr>
              <th></th>
              <th>Jenis</th>
              <th>Nama</th>
              <th>Outlet</th>
              <th>Tanggal</th>
              <th>Perihal</th>
              <th>Divisi</th>
              <th>Catatan</th>
              <th>Status</th>
              <th>By Divisi</th>
              <th>Note</th>
              <th>By Manager</th>
              <th>Note</th>
              <th>By Keuangan</th>
              <th>Note</th>
              <th>Foto</th>
              <th>Actions</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
    <!--end::Body-->
  </div>
  <!-- list section end -->
</section>
<!-- users list ends -->
@endsection

@section('vendor-script')
  {{-- Vendor js files --}}
  <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap4.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>

<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js" type="text/javascript"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script type="text/javascript">
    $(function () {
      var table = $('.yajra-datatable').DataTable({
          processing: true,
          serverSide: true,
          ajax: "{{ route('pengadaan.table', $type) }}",
          dom: 'lBfrtip',
          buttons: [
            'excel', 'csv', 'pdf', 'copy', 'print'
          ],
          columns: [
              {data: 'id', name: 'id'},
              {data: 'jenis', name: 'jenis'},
              {data: 'nama', name: 'nama'},
              {data: 'divisi_name', name: 'divisi_name'},
              {data: 'tanggal', name: 'tanggal'},
              {data: 'perihal', name: 'perihal'},
              {data: 'outlet_name', name: 'outlet_name'},
              {data: 'catatan', name: 'catatan'},
              {data: 'status', name: 'status'},
              {data: 'bydivisi', name: 'bydivisi'},
              {data: 'note1', name: 'note1'},
              {data: 'bymanager', name: 'bymanager'},
              {data: 'note2', name: 'note2'},
              {data: 'bykeuangan', name: 'bykeuangan'},
              {data: 'note3', name: 'note3'},
              {data: 'foto', name: 'foto'},
              {
                  data: 'action', 
                  name: 'action', 
                  orderable: true, 
                  searchable: true
              },
          ]
      });
      
    });

  </script>
@endsection
