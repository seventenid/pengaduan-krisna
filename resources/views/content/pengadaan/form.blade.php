@extends('layouts/contentLayoutMaster')

@section('title')
  {{$page_title}}
@endsection

@section('vendor-style')
  {{-- Vendor Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-user.css')) }}">

  <link rel="stylesheet" href="{{ asset('css/summernote/summernote.min.css') }}">
@endsection

@section('content')
<!-- users edit start -->
<section class="app-user-edit">
  <div class="card">
    <div class="card-body">

      <h1>Form Pengadaan</h1>
      </br></br>

        <!-- Form1 Tab starts -->
        <div class="tab-pane active" id="form1" aria-labelledby="form1-tab" role="tabpanel">

          @include("_includes.alert")

          @php $disabled = $page_type == 'view' ? 'disabled' : '' @endphp

          <!-- users edit account form start -->
          <form class="form-validate" id="Form_Input" method="{{ !empty($method) ? $method : 'POST' }}" action="{{ $form_action }}" enctype="multipart/form-data">
            @csrf
            @if(empty($method))
              {{ $page_type == 'create' ? '' : method_field('PUT') }}
            @endif

           
            <div class="row">

            <div class="col-md-4">
                <div class="form-group">
                  <label for="jenis">Jenis</label>
                    {!! Form::select('jenis', 
                      ['CAPEX' => 'CAPEX', 'OPEX' => 'OPEX'], 
                      !empty($item->jenis) ? $item->jenis : old('jenis'),
                      ['id' => 'jenis', 'class' => 'form-control basic-select2']
                    ); !!}
                    
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label for="nama">Nama</label>
                  <input
                    type="text"
                    class="form-control @error('nama') is-invalid @enderror"
                    value="{{ !empty($item->nama) ? $item->nama : old('nama') }}"
                    name="nama"
                    id="nama"
                    {{ $disabled }}
                  />
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label for="dari_divisi_outlet">Dari Team</label>

                    <input
                      type="text"
                      class="form-control @error('divisi') is-invalid @enderror"
                      value="{{ auth()->user()->divisitable->divisi ?? '' }}"
                      name="divisi"
                      id="divisi"
                      readonly
                    />

                    <input
                      type="hidden"
                      class="form-control @error('dari_divisi_outlet') is-invalid @enderror"
                      value="{{ auth()->user()->divisitable->id ?? '' }}"
                      name="dari_divisi_outlet"
                      id="dari_divisi_outlet"
                    />
                    
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label for="tanggal">Tanggal</label>
                  <input
                    type="date"
                    class="form-control @error('tanggal') is-invalid @enderror"
                    value="{{ !empty($item->tanggal) ? $item->tanggal : old('tanggal') }}"
                    name="tanggal"
                    id="tanggal"
                    {{ $disabled }}
                  />
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label for="perihal">Perihal</label>
                  <input
                    type="text"
                    class="form-control @error('perihal') is-invalid @enderror"
                    value="{{ !empty($item->perihal) ? $item->perihal : old('perihal') }}"
                    name="perihal"
                    id="perihal"
                    {{ $disabled }}
                  />
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label for="ke_outlet">Untuk Team</label>
                    {!! Form::select('ke_outlet', 
                      $tujuan_options, 
                      auth()->user()->outlettable->outlet ?? '',
                      ['id' => 'ke_outlet', 'class' => 'form-control basic-select2']
                    ); !!}
                    
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label for="catatan">Catatan</label>
                  <textarea
                    class="form-control summernote @error('catatan') is-invalid @enderror"
                    name="catatan"
                    id="catatan"
                    rows="4"
                    {{ $disabled }}
                  >{{ !empty($item->catatan) ? $item->catatan : old('catatan') }}</textarea>
                </div>
              </div>

              <div class="col-12 d-flex flex-sm-row flex-column mt-2">
                @if ($page_type == "edit" || $page_type == "create")
                <button type="submit" class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1">{{ $page_type == 'create' ? 'Create Pengaduan' : 'Save Changes' }}</button>
                <button type="reset" class="btn btn-outline-secondary mb-1 mb-sm-0 mr-0 mr-sm-1">Reset</button>
                @endif
                
              </div>
            </div>
          </form>
          <!-- users edit account form ends -->
        </div>
        <!-- Form1 Tab ends -->

      </div>

    </div>
  </div>
</section>
<!-- users edit ends -->
@endsection

@section('vendor-script')
  {{-- Vendor js files --}}
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/components/components-navs.js')) }}"></script>
  <script src="{{ asset('css/summernote/summernote.min.js') }}"></script>

  <script>
    $(document).ready(function() {
      $('.summernote').summernote();
    });
  </script>

  <script type="text/javascript">
    $(function () {
      'use strict';

      //Select2-------------------------
      $(".basic-select2").select2();
      //Select2-------------------------

      // Change user profile picture-------------------------
      var changePicture = $('#foto');
      var userAvatar = $('.user-avatar');

      if (changePicture.length) {
        $(changePicture).on('change', function (e) {
          var reader = new FileReader(),
            files = e.target.files;
          reader.onload = function () {
            if (userAvatar.length) {
              userAvatar.attr('src', reader.result);
            }
          };
          reader.readAsDataURL(files[0]);
        });
      }
      // Change user profile picture-------------------------

      // Validation-------------------------
      var form = $('.form-validate');
      if (form.length) {
        $(form).each(function () {
          var $this = $(this);
          $this.validate({
            submitHandler: function (form, event) {
              form.submit();
            },
            rules: {
              name: {
                required: true
              },
            }
          });
        });
      }
      // Validation-------------------------
      
    });
  </script>
@endsection
