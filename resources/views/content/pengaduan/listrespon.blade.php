@extends('layouts/contentLayoutMaster')

@section('title')
  {{$page_title}}
@endsection

@section('vendor-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap4.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
@endsection

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-user.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
@endsection

@section('content')
<!-- users list start -->
<section class="app-user-list">
  <!-- list section start -->
  <div class="card">

    @include("_includes.alert")

    <!--begin::Header-->
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
      <div class="card-title">
        <h3 class="card-label">{{$page_title}}</h3>
      </div>
      <div class="card-toolbar">
        <!--begin::Button-->
        <!--<a href="{{route('company.create')}}" class="btn btn-primary font-weight-bolder">
        <span class="fa fa-plus-circle"></span> Add Data</a>-->
        <!--end::Button-->
      </div>
    </div>
    <!--end::Header-->


    <!--begin::Body-->
    <div class="card-body">
      <div class="card-datatable table-responsive">
        <table class="table table-bordered yajra-datatable">
          <thead class="thead-light">
            <tr>
              <th>NO</th>
              <th>Nama</th>
              <th>Dari Team</th>
              <th>Untuk Team</th>
              <th>Tanggal</th>
              <th>Foto</th>
              <th>Status</th>
              <th>Keterangan</th>
              <th>Estimasi</th>
              <th>Done</th>
              <th>Note</th>
              <th>Actions</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
    <!--end::Body-->
  </div>
  <!-- list section end -->
</section>
<!-- users list ends -->
<div class="modal fade" id="note-modal" tabindex="-1" role="dialog" aria-labelledby="note-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="note-modal-label">Note</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
          </button>
        </div>
      
      <form id="note-form">
      <!-- id -->
      
      <input type="hidden" name="id" class="form-control" id="modal-note-id" required>
      <div class="col-md-4">
          <div class="form-group">
            </br>
            <h6>Note</h6>
            <input type="hidden" id="noteId" name="id">
            <textarea class="form-control @error('tgl') is-invalid @enderror" name="note"
              id="note" ></textarea>
            
          </div>
        </div>
      <!-- /id -->
      <div class="modal-footer">
        <button type="button" id="note_button" class="btn btn-danger" >Proses</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      </form>

      </div>
    </div>
  </div>
@endsection

@section('vendor-script')
  {{-- Vendor js files --}}
  <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap4.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>

<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js" type="text/javascript"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script type="text/javascript">
    $(function () {
      var table = $('.yajra-datatable').DataTable({
          processing: true,
          serverSide: true,
          ajax: "{{ route('pengaduan.listrespon.table') }}",
          dom: 'lBfrtip',
          buttons: [
            'excel', 'csv', 'pdf', 'copy', 'print'
          ],
          columns: [
              {data: 'id', name: 'id'},
              {data: 'nama', name: 'nama'},
              {data: 'divisi_name', name: 'divisi_name'},
              {data: 'outlet_name', name: 'outlet_name'},
              {data: 'tgl', name: 'tgl'},
              {data: 'foto', name: 'foto'},
              {data: 'status', name: 'status'},
              {data: 'keterangan', name: 'keterangan'},
              {data: 'estimasi', name: 'estimasi'},
              {data: 'donedate', name: 'donedate'},
              {data: 'note1', name: 'note1'},
              {
                  data: 'action', 
                  name: 'action', 
                  orderable: true, 
                  searchable: true
              },
          ]
      });
      
    });

  </script>
  
  <script type="text/javascript">
      $(document).on('click', ".note-item", function() {
          $tr = $(this).attr('data-href');
          $id = $(this).attr('data-id');
          
          $('#modal-note-id').val($tr);
          $('#noteId').val($id);
          
          $('#note-modal').modal('show');
      });

      // on modal hide
      $('#note-modal').on('hide.bs.modal', function() {
          $('.note-item-trigger-clicked').removeClass('note-item-trigger-clicked')
          $('#note-form').trigger('reset');
      });

      //ON MODAL DELETE SUBMITED
      $(document).on('click', "#note_button", function() {
          var urlx = $('#modal-note-id').val();
          var data = $('#note-form').serializeArray();
          $.ajax({
              type:'POST',
              url: urlx,
              data: data,
              success:function(data) {
                  if(data.type == "Success"){
                    toastr.success(data.msg);
                  }else{
                    toastr.error(data.msg);
                  }

                  var table = $('.yajra-datatable').DataTable();
                  table.ajax.reload();		
                  $('#note-modal').modal('toggle');
              }
          });
      });
  </script>
@endsection
