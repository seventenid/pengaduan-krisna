@extends('layouts/contentLayoutMaster')

@section('title', 'User Edit')

@section('vendor-style')
  {{-- Vendor Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-user.css')) }}">

  <link rel="stylesheet" href="{{ asset('css/summernote/summernote.min.css') }}">
@endsection

@section('content')
<!-- users edit start -->
<section class="app-user-edit">
  <div class="card">
    <div class="card-body">

      <ul class="nav nav-pills" role="tablist">
        <li class="nav-item">
          <a
            class="nav-link d-flex align-items-center active"
            id="form1-tab"
            data-toggle="tab"
            href="#form1"
            aria-controls="form1"
            role="tab"
            aria-selected="true"
          >
            <i data-feather="user"></i><span class="d-none d-sm-block">{{$page_title}}</span>
          </a>
        </li>
      </ul>

        <!-- Form1 Tab starts -->
        <div class="tab-pane active" id="form1" aria-labelledby="form1-tab" role="tabpanel">

          @include("_includes.alert")

          @php $disabled = $page_type == 'view' ? 'disabled' : '' @endphp

          <!-- users edit account form start -->
          <form class="form-validate" id="Form_Input" method="{{ !empty($method) ? $method : 'POST' }}" action="{{ $form_action }}" enctype="multipart/form-data">
            @csrf
            @if(empty($method))
              {{ $page_type == 'create' ? '' : method_field('PUT') }}
            @endif

            <div class="media mb-2">
              <img
                src="../../../alhikmah/public/{{ $item->logo }}"
                alt="users avatar"
                class="user-avatar users-avatar-shadow rounded mr-2 my-25 cursor-pointer"
                style="max-width:600px"
              />
              <div class="media-body mt-50">
                <div class="col-12 d-flex mt-1 px-0">
                  <label class="btn btn-primary mr-75 mb-0" for="change-picture">
                    <span class="d-none d-sm-block">Change Picture</span>
                    <input
                      name="logo"
                      class="form-control"
                      type="file"
                      id="change-picture"
                      hidden
                      accept="image/png, image/jpeg, image/jpg"
                    />
                    <span class="d-block d-sm-none">
                      <i class="mr-0" data-feather="edit"></i>
                    </span>
                  </label>
                </div>
              </div>
            </div>

            <div class="row">
            
              <div class="col-md-4">
                <div class="form-group">
                  <label for="name">Nama</label>
                  <input
                    type="text"
                    class="form-control @error('name') is-invalid @enderror"
                    value="{{ !empty($item->name) ? $item->name : old('name') }}"
                    name="name"
                    id="name"
                    {{ $disabled }}
                  />
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label for="phone_1">Telepon 1</label>
                  <input
                    type="text"
                    class="form-control @error('phone_1') is-invalid @enderror"
                    value="{{ !empty($item->phone_1) ? $item->phone_1 : old('phone_1') }}"
                    name="phone_1"
                    id="phone_1"
                    {{ $disabled }}
                  />
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label for="email_1">Email 1</label>
                  <input
                    type="email"
                    class="form-control @error('email_1') is-invalid @enderror"
                    value="{{ !empty($item->email_1) ? $item->email_1 : old('email_1') }}"
                    name="email_1"
                    id="email_1"
                    {{ $disabled }}
                  />
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label for="phone_2">Telepon 2</label>
                  <input
                    type="text"
                    class="form-control @error('phone_2') is-invalid @enderror"
                    value="{{ !empty($item->phone_2) ? $item->phone_2 : old('phone_2') }}"
                    name="phone_2"
                    id="phone_2"
                    {{ $disabled }}
                  />
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label for="email_2">Email 2</label>
                  <input
                    type="email"
                    class="form-control @error('email_2') is-invalid @enderror"
                    value="{{ !empty($item->email_2) ? $item->email_2 : old('email_2') }}"
                    name="email_2"
                    id="email_2"
                    {{ $disabled }}
                  />
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                </div>
              </div>


              <div class="col-md-6">
                <div class="form-group">
                  <label for="description">Desktipsi Perusahaan</label>
                  <textarea
                    class="form-control summernote @error('description') is-invalid @enderror"
                    name="description"
                    id="description"
                    rows="4"
                    {{ $disabled }}
                  >{{ !empty($item->description) ? $item->description : old('description') }}</textarea>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="address">Alamat</label>
                  <textarea
                    class="form-control summernote @error('address') is-invalid @enderror"
                    name="address"
                    id="address"
                    rows="4"
                    {{ $disabled }}
                  >{{ !empty($item->address) ? $item->address : old('address') }}</textarea>
                </div>
              </div>

              <div class="col-12 d-flex flex-sm-row flex-column mt-2">
                @if ($page_type == "edit" || $page_type == "create")
                <button type="submit" class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1">{{ $page_type == 'create' ? 'Create Company' : 'Save Changes' }}</button>
                <button type="reset" class="btn btn-outline-secondary mb-1 mb-sm-0 mr-0 mr-sm-1">Reset</button>
                @endif
                <a href="{{ route('company.index') }}" class="btn btn-danger btn-outline-secondary">Back</a>
              </div>
            </div>
          </form>
          <!-- users edit account form ends -->
        </div>
        <!-- Form1 Tab ends -->

      </div>

    </div>
  </div>
</section>
<!-- users edit ends -->
@endsection

@section('vendor-script')
  {{-- Vendor js files --}}
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/components/components-navs.js')) }}"></script>
  <script src="{{ asset('css/summernote/summernote.min.js') }}"></script>

  <script>
    $(document).ready(function() {
      $('.summernote').summernote();
    });
  </script>

  <script type="text/javascript">
    $(function () {
      'use strict';

      //Select2-------------------------
      $(".basic-select2").select2();
      //Select2-------------------------

      // Change user profile picture-------------------------
      var changePicture = $('#change-picture');
      var userAvatar = $('.user-avatar');

      if (changePicture.length) {
        $(changePicture).on('change', function (e) {
          var reader = new FileReader(),
            files = e.target.files;
          reader.onload = function () {
            if (userAvatar.length) {
              userAvatar.attr('src', reader.result);
            }
          };
          reader.readAsDataURL(files[0]);
        });
      }
      // Change user profile picture-------------------------

      // Validation-------------------------
      var form = $('.form-validate');
      if (form.length) {
        $(form).each(function () {
          var $this = $(this);
          $this.validate({
            submitHandler: function (form, event) {
              form.submit();
            },
            rules: {
              name: {
                required: true
              },
            }
          });
        });
      }
      // Validation-------------------------
      
    });
  </script>
@endsection
