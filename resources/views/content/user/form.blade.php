@extends('layouts/contentLayoutMaster')

@section('title')
  {{$page_title}}
@endsection

@section('vendor-style')
  {{-- Vendor Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-user.css')) }}">

  <link rel="stylesheet" href="{{ asset('css/summernote/summernote.min.css') }}">
@endsection

@section('content')
<!-- users edit start -->
<section class="app-user-edit">
  <div class="card">
    <div class="card-body">

    <h1>Form User</h1>
      </br></br>

        <!-- Form1 Tab starts -->
        <div class="tab-pane active" id="form1" aria-labelledby="form1-tab" role="tabpanel">

          @include("_includes.alert")

          @php $disabled = $page_type == 'view' ? 'disabled' : '' @endphp

          <!-- users edit account form start -->
          <form class="form-validate" id="Form_Input" method="{{ !empty($method) ? $method : 'POST' }}" action="{{ $form_action }}">
            @csrf
            @if(empty($method))
              {{ $page_type == 'create' ? '' : method_field('PUT') }}
            @endif

            <div class="row">
            
              <div class="col-md-4">
                <div class="form-group">
                  <label for="name">Nama</label>
                  {!! Form::text('name', 
                    !empty($item->name) ? $item->name : old('name'),
                    ['id' => 'name', 'class' => 'form-control', 'disabled' => $disabled ? true : false]
                  ); !!}
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label for="email">E-mail</label>
                  {!! Form::text('email', 
                    !empty($item->email) ? $item->email : old('email'),
                    ['id' => 'email', 'class' => 'form-control', 'disabled' => $disabled ? true : false]
                  ); !!}
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label for="password">Password</label>
                  
                  @if ($page_type == "edit")
                  <button type="button" name="show-password" id="show-password-button" class="btn btn-outline-info form-control ">Change</button>
                  @endif

                  <div id="password-field" class="{{ $page_type == 'edit' ? 'd-none' : '' }}">
                    <div class="input-group form-password-toggle input-group-merge">
                      <input 
                        type="password" 
                        class="form-control @error('password') is-invalid @enderror" 
                        placeholder="Min 8 Character Password"
                        value="{{ !empty($item->password) ? $item->password : old('password') }}" 
                        name="password" 
                        id="password"
                        autocomplete="on"
                        {{ $disabled }} 
                      />
                      <div class="input-group-append">
                        <div class="input-group-text cursor-pointer">
                          <i data-feather="eye"></i>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label for="roles">Roles</label>
                  <input
                    type="text"
                    class="form-control @error('roles') is-invalid @enderror"
                    value="{{ !empty($item->roles) ? $item->roles : old('roles') }}"
                    name="roles"
                    id="roles"
                    {{ $disabled }}
                  />
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label for="divisi">Divisi</label>
                    {!! Form::select('divisi', 
                      $divisi_options, 
                      !empty($item->divisi) ? $item->divisi : old('divisi'), 
                      ['id' => 'divisi', 'class' => 'form-control basic-select2', 'disabled' => $disabled ? true : false]
                    ); !!}
                </div>
              </div>

              

              <div class="col-12 d-flex flex-sm-row flex-column mt-2">
                @if ($page_type == "edit" || $page_type == "create")
                <button type="submit" class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1">{{ $page_type == 'create' ? 'Create User' : 'Save Changes' }}</button>
                <button type="reset" class="btn btn-outline-secondary mb-1 mb-sm-0 mr-0 mr-sm-1">Reset</button>
                @endif
                <a href="{{ route('user.index') }}" class="btn btn-danger btn-outline-secondary">Back</a>
              </div>
            </div>
          </form>
          <!-- users edit account form ends -->
        </div>
        <!-- Form1 Tab ends -->

      </div>

    </div>
  </div>
</section>
<!-- users edit ends -->
@endsection

@section('vendor-script')
  {{-- Vendor js files --}}
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/components/components-navs.js')) }}"></script>

  <script type="text/javascript">
    $(function () {
      'use strict';

      //Select2-------------------------
      $(".basic-select2").select2();
      //Select2-------------------------

      // toggle password show-------------------------
      $('#show-password-button').click(function (e) {
          if($("#password-field").hasClass("d-none")){
              $('#password-field').removeClass('d-none');
              // to always uncheck the checkbox after button click
              $("#password").prop("type", "password");
              $("#password").val('');
          }else{
              $('#password-field').addClass('d-none');
              $("#password").val('');
          }
      });
      // toggle password show-------------------------

      // toggle password in plaintext if checkbox is selected-------------------------
      $("#show-password").click(function () {
          $(this).is(":checked") ? $("#password").prop("type", "text") : $("#password").prop("type", "password");
      });
      // toggle password in plaintext if checkbox is selected-------------------------

      // Validation-------------------------
      var form = $('.form-validate');
      if (form.length) {
        $(form).each(function () {
          var $this = $(this);
          $this.validate({
            submitHandler: function (form, event) {
              form.submit();
            },
            rules: {
              name: {
                required: true
              },
              email: {
                required: true,
                email: true
              },
              password: {
                required: true,
                minlength: 8
              }
            }
          });
        });

      }
      // Validation-------------------------
      
    });
  </script>
@endsection
