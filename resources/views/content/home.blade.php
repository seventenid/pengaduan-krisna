@extends('layouts/contentLayoutMaster')

@section('title', 'Home')

@section('content')
<!-- Kick start -->
<div class="card">
  <div class="card-header">
    <h4 class="card-title">Pengaduan</h4>
  </div>
  
</div>
<div class="row">
@foreach($data->divisis as $divisi)
<div class="col-3 ">
  <div class="card card-statistics">
    <div class="card-header">
      <h4 class="card-title">{{$divisi->divisi}}</h4>
    </div>
    <div class="card-body statistics-body">
      <div class="row">
        <div class="col-4 ">
          <div class="d-flex flex-row">
          <div class="avatar bg-light-info me-2">
              <div class="avatar-content">
                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user avatar-icon"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
              </div>
            </div>
            <div class="my-auto">
              <h4 class="fw-bolder mb-0">{{ $divisi->pengaduanPending()->count() }}</h4>
              <p class="card-text font-small-3 mb-0">Pending</p>
            </div>
          </div>
        </div>
        <div class="col-4 ">
          <div class="d-flex flex-row">
            <div class="avatar bg-light-info me-2">
              <div class="avatar-content">
                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user avatar-icon"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
              </div>
            </div>
            <div class="my-auto">
              <h4 class="fw-bolder mb-0">{{ $divisi->pengaduanProses()->count() }}</h4>
              <p class="card-text font-small-3 mb-0">On Proses</p>
            </div>
          </div>
        </div>
        <div class="col-4 ">
          <div class="d-flex flex-row">
            <div class="avatar bg-light-info me-2">
              <div class="avatar-content">
                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user avatar-icon"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
              </div>
            </div>
            <div class="my-auto">
              <h4 class="fw-bolder mb-0">{{ $divisi->pengaduanFinish()->count() }}</h4>
              <p class="card-text font-small-3 mb-0">Complete</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endforeach
</div>



<!-- Pengadaan start -->
<div class="card">
  <div class="card-header">
    <h4 class="card-title">Pengadaan</h4>
  </div>
  
</div>
<div class="row">
@foreach($data->divisis as $divisi)
<div class="col-3 ">
  <div class="card card-statistics">
    <div class="card-header">
      <h4 class="card-title">{{$divisi->divisi}}</h4>
    </div>
    <div class="card-body statistics-body">
      <div class="row">
        <div class="col-6 ">
          <div class="d-flex flex-row">
          <div class="avatar bg-light-info me-2">
              <div class="avatar-content">
                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user avatar-icon"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
              </div>
            </div>
            <div class="my-auto">
              <h4 class="fw-bolder mb-0">{{ $divisi->pengadaanPending()->count() }}</h4>
              <p class="card-text font-small-3 mb-0">Pending</p>
            </div>
          </div>
        </div>
        <div class="col-6 ">
          <div class="d-flex flex-row">
            <div class="avatar bg-light-info me-2">
              <div class="avatar-content">
                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user avatar-icon"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
              </div>
            </div>
            <div class="my-auto">
              <h4 class="fw-bolder mb-0">{{ $divisi->pengadaanFinish()->count() }}</h4>
              <p class="card-text font-small-3 mb-0">Complete</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endforeach
</div>
<!-- Pengadaan end -->

@endsection
