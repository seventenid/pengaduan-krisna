<?php
?>

<body class="horizontal-layout horizontal-menu {{$configData['horizontalMenuType']}} {{ $configData['showMenu'] === true ? '' : '1-column' }}
{{ $configData['blankPageClass'] }} {{ $configData['bodyClass'] }}
{{ $configData['footerType'] }}" data-menu="horizontal-menu" data-col="{{ $configData['showMenu'] === true ? '' : '1-column' }}" data-open="hover" data-layout="{{ ($configData['theme'] === 'light') ? '' : $configData['layoutTheme'] }}" style="{{ $configData['bodyStyle'] }}" data-framework="laravel" data-asset-path="{{ asset('/')}}">

  <!-- BEGIN: Header-->
  {{-- Include Navbar --}}
  @include('panels.navbar')

  {{-- Include Sidebar --}}
  @if((isset($configData['showMenu']) && $configData['showMenu'] === true))
  @include('panels.horizontalMenu')
  @endif

  <!-- BEGIN: Content-->
  <div class="app-content content {{ $configData['pageClass'] }}">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    @if(($configData['contentLayout']!=='default') && isset($configData['contentLayout']))
    <div class="content-area-wrapper {{ $configData['layoutWidth'] === 'boxed' ? 'container p-0' : '' }}">
      <div class="{{ $configData['sidebarPositionClass'] }}">
        <div class="sidebar">
          {{-- Include Sidebar Content --}}
          @yield('content-sidebar')
        </div>
      </div>
      <div class="{{ $configData['contentsidebarClass'] }}">
        <div class="content-wrapper">
          <div class="content-body">
            {{-- Include Page Content --}}
            @yield('content')
          </div>
        </div>
      </div>
    </div>
    @else
    <div class="content-wrapper {{ $configData['layoutWidth'] === 'boxed' ? 'container p-0' : '' }}">
      {{-- Include Breadcrumb --}}
      @if($configData['pageHeader'] == true)
      @include('panels.breadcrumb')
      @endif

      <div class="content-body">

        {{-- Include Page Content --}}
        @yield('content')

      </div>
    </div>
    @endif

  </div>
  <!-- End: Content-->

  <div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="delete-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="delete-modal-label">Confirmation Delete Data</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
          </button>
        </div>
      
      <form id="delete-form">
      <!-- id -->
      <input type="hidden" name="modal-input-id" class="form-control" id="modal-input-id" required>
      <!-- /id -->
      <div class="modal-body">
        <div class="card text-white bg-dark mb-0">
          <div class="card-body">
            <Strong>Are You Sure You Want To Delete This Data? </br>
            The Deleted data cant be restored</strong>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" id="delete_button" class="btn btn-danger" >Delete</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      </form>

      </div>
    </div>
  </div>

  <div class="modal fade" id="estimasi-modal" tabindex="-1" role="dialog" aria-labelledby="estimasi-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="estimasi-modal-label">Estimasi</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
          </button>
        </div>
      
      <form id="estimasi-form">
      <!-- id -->
      
      <input type="hidden" name="id" class="form-control" id="modal-estimasi-id" required>
            <div class="col-md-4">
                <div class="form-group">
                  </br>
                  <h6>Masukkan Estimasi Pengerjaan </h6>
                  <input
                    type="number"
                    class="form-control @error('tgl') is-invalid @enderror"
                    name="estimasi"
                    id="estimasi" 
                  />
                </div>
              </div>
      <!-- /id -->
      <div class="modal-footer">
        <button type="button" id="estimasi_button" class="btn btn-danger" >Proses</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      </form>

      </div>
    </div>
  </div>

  <div class="sidenav-overlay"></div>
  <div class="drag-target"></div>

  {{-- include footer --}}
  @include('panels/footer')

  {{-- include default scripts --}}
  @include('panels/scripts')

  <script type="text/javascript">
    $(window).on('load', function() {
      if (feather) {
        feather.replace({
          width: 14
          , height: 14
        });
      }
    })

  </script>

  <script type="text/javascript">
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  </script>

  <script type="text/javascript">
      $(document).on('click', ".delete-item", function() {
          $tr = $(this).attr('data-href');
          
          $('#modal-input-id').val($tr);
          
          $('#edit-modal').modal('show');
      });

      // on modal hide
      $('#edit-modal').on('hide.bs.modal', function() {
          $('.delete-item-trigger-clicked').removeClass('delete-item-trigger-clicked')
          $('#edit-form').trigger('reset');
      });

      //ON MODAL DELETE SUBMITED
      $(document).on('click', "#delete_button", function() {
          var urlx = $('#modal-input-id').val();
          $.ajax({
              type:'DELETE',
              url: urlx,
              data: { "_token": "{{ csrf_token() }}" },
              success:function(data) {
                  if(data.type == "Success"){
                    toastr.success(data.msg);
                  }else{
                    toastr.error(data.msg);
                  }

                  var table = $('.yajra-datatable').DataTable();
                  table.ajax.reload();		
                  $('#delete-modal').modal('toggle');
              }
          });
      });
  </script>
  <script type="text/javascript">
      $(document).on('click', ".estimasi-item", function() {
          $tr = $(this).attr('data-href');
          
          $('#modal-estimasi-id').val($tr);
          
          $('#estimasi-modal').modal('show');
      });

      // on modal hide
      $('#estimasi-modal').on('hide.bs.modal', function() {
          $('.estimasi-item-trigger-clicked').removeClass('estimasi-item-trigger-clicked')
          $('#estimasi-form').trigger('reset');
      });

      //ON MODAL ESTIMASI SUBMITED
      $(document).on('click', "#estimasi_button", function() {
          
          var data = $('#estimasi-form').serializeArray();
          $.ajax({
              type:'POST',
              url: 'estimasi',
              data: data,
              success:function(data) {
                  if(data.type == "Success"){
                    toastr.success(data.msg);
                  }else{
                    toastr.error(data.msg);
                  }

                  var table = $('.yajra-datatable').DataTable();
                  table.ajax.reload();		
                  $('#estimasi-modal').modal('toggle');
              }
          });
      });
  </script>
</body>

</html>
