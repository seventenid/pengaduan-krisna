<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StaterkitController;
use App\Http\Controllers\LanguageController;

use App\Http\Controllers\UserController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\PengaduanController;
use App\Http\Controllers\PengadaanController;
use App\Http\Controllers\DivisiController;
use App\Http\Controllers\OutletController;
use App\Http\Controllers\ListresponController;
use App\Http\Controllers\ListResponPengadaanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth::routes();
Auth::routes();

Route::group(['middleware' => ['auth']], function () { 
    Route::get('/', [StaterkitController::class, 'home'])->name('home');
    Route::get('home', [StaterkitController::class, 'home'])->name('home');

    //COMPANY ROUTE
    Route::get('company/table', [CompanyController::class, 'tables'])->name('company.table');
    Route::resource('company', CompanyController::class);
    //---------------------------------------------
    //USER ROUTE
    Route::get('user/table', [UserController::class, 'tables'])->name('user.table');
    Route::resource('user', UserController::class);
    //---------------------------------------------
    //PENGADUAN ROUTE
    Route::get('pengaduan/table', [PengaduanController::class, 'tables'])->name('pengaduan.table');
    Route::get('pengaduan/done/{id}', [PengaduanController::class, 'done'])->name('pengaduan.done');
    Route::post('pengaduan/estimasi', [PengaduanController::class, 'estimasi'])->name('pengaduan.estimasi');
    Route::get('pengaduan/listrespon', [ListresponController::class, 'index'])->name('pengaduan.listrespon');
    Route::get('pengaduan/listrespon/table', [ListresponController::class, 'tables'])->name('pengaduan.listrespon.table');
    Route::post('pengaduan/input1', [ListresponController::class, 'input1'])->name('pengaduan.input1');
    Route::resource('pengaduan', PengaduanController::class);
    
    //---------------------------------------------
    //PENGADAAN ROUTE
    Route::get('pengadaan/table/{type}', [PengadaanController::class, 'tables'])->name('pengadaan.table');
    Route::get('pengadaan/listrespon', [ListResponPengadaanController::class, 'index'])->name('pengadaan.listrespon');
    Route::get('pengadaan/listrespon/table//{type}', [ListResponPengadaanController::class, 'tables'])->name('pengadaan.listrespon.table');
    
    Route::post('pengadaan/input1', [PengadaanController::class, 'input1'])->name('pengadaan.input1');
    Route::post('pengadaan/input2', [PengadaanController::class, 'input2'])->name('pengadaan.input2');
    Route::post('pengadaan/input3', [PengadaanController::class, 'input3'])->name('pengadaan.input3');
    Route::post('pengadaan/foto', [PengadaanController::class, 'foto'])->name('pengadaan.foto');
    Route::resource('pengadaan', PengadaanController::class);
    //---------------------------------------------
    //Divisi ROUTE
    Route::get('divisi/table', [DivisiController::class, 'tables'])->name('divisi.table');
    Route::resource('divisi', DivisiController::class);
    //---------------------------------------------
    //Outlet ROUTE
    Route::get('outlet/table', [OutletController::class, 'tables'])->name('outlet.table');
    Route::resource('outlet', OutletController::class);
    //---------------------------------------------


    // locale Route
    Route::get('lang/{locale}', [LanguageController::class, 'swap']);

});